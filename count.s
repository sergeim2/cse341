.globl main
.data
prompt:   .asciiz "Enter an unsigned integer: "    # Offset = 0
result0:  .asciiz "The number of zeros in the number is "     # Offset = 28
result1:  .asciiz "\nThe number of ones in the number is "    # Offset = 66
.text
main:   add $s2, $ra, $0     # Save ra into s2

        lui $a0, 0x1000      # Print prompt
        addi $v0, $0, 4
        syscall

        addi $v0, $0, 5      # Get integer
        syscall
        add $t0, $0, $v0

# Call count

        add $a0, $t0, $0     # Pass in user entered number
        jal count
        nor $0, $0, $0       # Branch delay slot

# Save returned values from count
        add $s0, $0, $v0     # Number of zeros
        add $s1, $0, $v1     # Number of ones

        lui $a0, 0x1000      # Print number of zeros
        addi $a0, $a0, 28
        addi $v0, $0, 4
        syscall

        add $a0, $s0, $0
        addi $v0, $0, 1
        syscall

        lui $a0, 0x1000      # Print number of ones
        addi $a0, $a0, 66
        addi $v0, $0, 4
        syscall

        add $a0, $s1, $0
        addi $v0, $0, 1
        syscall

        add $ra, $s2, $0     # Restore return address
        jr $ra
        nor $0, $0, $0       # Branch delay slot

count:  # The number is passed in a0
        # The ones count is returned in v1 and the zeros count in v0

        addi $t1, $0, 32      # t1 is counter
        add $v1, $0, $0      # Initialization
check:  add $t0, $a0, $0     # Make a working copy of data        
        andi $t0, $t0, 1     # Isolate lsb
        add $v1, $v1, $t0    # Increment ones count
        addi $t1, $t1, -1    # Decrement count
        srl $a0, $a0, 1
        bne $t1, $0, check
        nor $0, $0, $0       # Branch Delay Slot
        addi $t2, $0, 32
        sub $v0, $t2, $v1
        jr $ra
        nor $0, $0, $0       # Branch Delay slot
                

