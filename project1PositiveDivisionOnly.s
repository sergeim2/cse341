#program dive/median/mean    only does positive division
#sergei mellow 50030322

.globl main

.data
.space 400
prompt: .asciiz "\nPlease select program: mean(1) median(2) division(3)"  #offset 400
numpromt: .asciiz "\nPlease Enter a number:"			        #offset 454
qprompt: .asciiz "\nDo you want to enter another number? Yes(1) No(0):" #offset 478
modep: .asciiz "\nThe division of these numbers:"		     	#offset 530
medip: .asciiz "\nThe median of these numbers:"				#offset 562 
meanp: .asciiz "\nThe mean of these numbers:" 				#offset 592
remap: .asciiz "\nThe remainder of these numbers:"                      #offset 620
dnum: .asciiz "\nPlease enter the number you want to divide"		#offset 653
divd: .asciiz "\nPlease enter the number you are dividing with" 	#offset 697
neg: .asciiz "-"         #offset 744
 
.text
main:
 add $v0,$0,$0  #resetting all registers for when program repeats
 add $v1,$0,$0
 add $a0,$0,$0
 add $a1,$0,$0
 add $a2,$0,$0
 add $a3,$0,$0
 add $t0,$0,$0
 add $t1,$0,$0
 add $t2,$0,$0
 add $t3,$0,$0
 add $t4,$0,$0
 add $t5,$0,$0
 add $t6,$0,$0
 add $t7,$0,$0
 add $s0,$0,$0
 add $s1,$0,$0
 add $s2,$0,$0
 add $s3,$0,$0
 add $s4,$0,$0
 add $s5,$0,$0
 add $s6,$0,$0
 add $s7,$0,$0
   add $s0, $ra, $0     #save ra in s0
   lui $a0, 0x1000  #Promt for medi mode mean
   addi $a0, $a0, 400
   addi $v0, $0, 4    
   syscall

   addi $v0, $0, 5  #get int input from user
   syscall
   add $t0, $v0, $0
   beq $t0, 1, mean
   nor $0, $0, $0       # Branch delay slot
   beq $t0, 2, median
   nor $0, $0, $0       # Branch delay slot
   beq $t0, 3, division
   nor $0, $0, $0       # Branch delay slot
   #addi $v1, $0, 5
 
median:
   addi $t7,$t7,50	#limit for inputs
   ###lui $s1, 0x1000	#address for second  array
   ###addi $s1, $0,200	#address of second array
   add $t5,$0,$0        #counter of inputs
   lui $t6,0x1000        #used to store values for temp
   lui $t0, 0x1000	#this t0 is for indexing the array
#setting up first value before looping
   lui $a0, 0x1000      # promt for number
   addi $a0, $a0, 454
   addi $v0, $0, 4
   syscall
   addi $v0, $0, 5  #get int input from user
   syscall
   sw $v0, ($t0)
   add $s1,$0,$v0    #value for checking whether a swap should happen
   #addi $t5,$t5,1
   #addi $t0,$t0,4       #adding 1 word  for 1 input
   aagain:               #input loop making array!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   lui $a0, 0x1000      # promt for number
   addi $a0, $a0, 478
   addi $v0, $0, 4
   syscall
   addi $v0, $0, 5  #get 1 or 0 to check if we should keep looping
   syscall
   beq $v0,0, arrange
   nor $0,$0,$0

   lui $a0, 0x1000      # promt for number
   addi $a0, $a0, 454
   addi $v0, $0, 4
   syscall
   addi $v0, $0, 5  #get int input from user
   syscall
  ############# #add $s1,$0,$v0
   slt $t3,$v0,$s1
   
   addi $t5,$t5,1   #inc index
   addi $t0,$t0,4   #inc mem address
   add $t4,$0,$t0  #backing up both values incase of swap
   add $t6,$0,$t5  #^
   sw $v0, ($t0)
# 
   beq $t3,1,swap
   nor $0,$0,$0
   add $s1,$0,$v0              
   sw $v0, ($t0)
   beq $t5,50, arrange #if 50 inputs are entered
   nor $0,$0,$0
   bne $v0,$0, aagain
   nor $0,$0,$0
   j aagain
   nor $0,$0,$0 
swap:			#my sorting algorithm
   add $s5, $0,$0
   lw $s6, ($t4)
   nor $0,$0,$0
   lw $s7, -4($t4)
   nor $0,$0,$0
   sw $s6, -4($t4)
   nor $0,$0,$0
   sw $s7, ($t4)
   nor $0,$0,$0
   addi $t6,$t6,-1
   beq $t6,0, aagain
   nor $0,$0,$0
   lw $a3, -8($t4)
   addi $t4,$t4 -4
   slt $s5,$s6,$a3
   beq $s5,1,swap
   nor $0,$0,$0  
   beq $s5,0,aagain
   nor $0,$0,$0
   beq $t6,0,aagain 
   nor $0,$0,$0

  arrange:		#done with sorting I am dividing number of inputs by two and doing different stuff for odd/even
   add $v1,$0,$0
   addi $t5,$t5,1
   add $t2,$0,$t5
   addi $t1,$0,2
   aloop:  sub $v0, $t2,$t1 #dividing loop
   nor $0, $0, $0
   bltz $v0, finish
   addi $v1,$v1,1
   sub $t2,$t2,$t1
   slt $t3, $t2, $t1    #sets t3 to 1 if big value smaller than divider
   bne $t3,$0, finish
   j aloop
   nor $0,$0,$0               
   finish:                   #v1=ans division   t2=ans remainder
   beq $t2,0,even  #if remainder is 0 then its even
   nor $0,$0,$0
   add $s5,$0,$0  #counter for odd loop
   lui $t4,0x1000
   oddloop:
   addi $t4,$t4,4
   addi $s5,$s5,1 
   bne $v1,$s5,oddloop
   nor $0,$0,$0
  #####beq $t2,$s5,print
   beq $v1,$s5,print
   lw $t2,($t4)
   #####j print
   even:
   lui $t4, 0x1000
   nor $0,$0,$0
   add $s5,$0,$0  #counter for odd loop
   evenloop:
   addi $t4,$t4,4
   addi $s5,$s5,1
   bne $s5,$v1,evenloop
   nor $0,$0,$0
   beq $s5,$v1,printeven
   lw $t2,($t4)
   printeven:
   lw $t7,-4($t4)
   nor $0,$0,$0
   add $t2,$t2,$t7
   addi $t1, $0,2
   add  $v1,$0,$0
   faloop:  sub $v0, $t2,$t1 #dividing loop
   nor $0, $0, $0
   bltz $v0, print
   addi $v1,$v1,1
   sub $t2,$t2,$t1
   slt $t3, $t2, $t1    #sets t3 to 1 if big value smaller than divider
   bne $t3,$0, print
   j faloop
   nor $0,$0,$0


   print:   
   lui $a0, 0x1000      # Print median
   addi $a0, $a0, 562
   addi $v0, $0, 4
   syscall
   lui $a0, 0x1000      #ands of median
   add $a0, $0, $v1
   addi $v0, $0, 1
   syscall

   j main
mean:
   add $t5,$0,$0        #counter of inputs
   add $t6,$0,$0	# t6 stores sum of total
   again: #input loop! t6=running sume
   lui $a0, 0x1000      # promt for number
   addi $a0, $a0, 454
   addi $v0, $0, 4
   syscall
   addi $v0, $0, 5  #get int input from user 
   syscall
   add $t6,$t6,$v0   #running some of inputs 
   addi $t5,$t5,1
   lui $a0, 0x1000      # promt for number
   addi $a0, $a0, 478
   addi $v0, $0, 4
   syscall
   
   addi $v0, $0, 5  #get 1 or 0 to check if we should keep looping
   syscall
   bne $v0,$0, again
   nor $0,$0,$0
   add $t2,$t6,$0
   add $t1,$t5,$0
   #division
   ddloop:  sub $v0, $t2,$t1 #dividing loop
   nor $0, $0, $0
   bltz $v0, ddone
   addi $v1,$v1,1
   sub $t2,$t2,$t1
   slt $t3, $t2, $t1    #sets t3 to 1 if big value smaller than divider
   bne $t3,$0, ddone
   j ddloop
   ddone:
   lui $a0, 0x1000      # Print mean
   addi $a0, $a0, 592
   addi $v0, $0, 4
   syscall
   lui $a0, 0x1000      #ans of division
   add $a0, $0, $v1 ## bug here fix it ASDIJBKHJSAOPDIJBN BSANJIODB NASKJDN
   addi $v0, $0, 1
   syscall
   j main
   nor $0, $0, $0
division:
   lui $a0, 0x1000      # promt for number to divide
   addi $a0, $a0, 653
   addi $v0, $0, 4
   syscall
   addi $v0, $0, 5  #get int input from user num to divide
   syscall
   add $t2,$v0,$0   #settting t2 to num to divide
   lui $a0, 0x1000      # promt for divider
   addi $a0, $a0, 697
   addi $v0, $0, 4
   syscall
   addi $v0, $0, 5  	#getting divider
   syscall
   add $t1,$v0,$0    	#Setting t1 to divider
   nor $0, $0, $0
   add $v1, $0, $0 	#division ans also known as counter
   nor $0, $0, $0       # Branch delay slot
   dloop:  sub $v0, $t2,$t1 #dividing loop
   nor $0, $0, $0
   bltz $v0, done
   addi $v1,$v1,1
   sub $t2,$t2,$t1
   slt $t3, $t2, $t1 	#sets t3 to 1 if big value smaller than divider
   bne $t3,$0, done
   j dloop
   done:
   nor $0,$0,$0
   lui $a0, 0x1000      # print divison
   addi $a0, $a0, 530
   addi $v0, $0, 4
   syscall
   lui $a0, 0x1000      #ans of division
   add $a0, $0, $v1 ## bug here fix it ASDIJBKHJSAOPDIJBN BSANJIODB NASKJDN 
   addi $v0, $0, 1
   syscall
   lui $a0, 0x1000      # Print remainder
   addi $a0, $a0, 620
   addi $v0, $0, 4
   syscall
   lui $a0, 0x1000      #ans of remainder
   add $a0, $0, $t2
   addi $v0, $0, 1
   syscall
   j main
   nor $0,$0,$0
