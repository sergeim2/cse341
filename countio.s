.globl main
.data
prompt:   .asciiz "Enter an unsigned integer: "    # Offset = 0
result0:  .asciiz "The number of zeros in the number is "     # Offset = 28
result1:  .asciiz "\nThe number of ones in the number is "    # Offset = 66
.text
main:   add $s2, $ra, $0     # Save ra into s2

        lui $a0, 0x1000      # Print prompt
        addi $v0, $0, 4
        syscall

        addi $v0, $0, 5      # Get integer
        syscall
        add $t0, $0, $v0

# Call count

        add $a0, $t0, $0     # Pass in user entered number
        jal count
        nor $0, $0, $0       # Branch delay slot

# Save returned values from count
        add $s0, $0, $v0     # Number of zeros
        add $s1, $0, $v1     # Number of ones

# The folliwng two lines are for debugging

        add $s0, $0, 17
        add $s1, $0, 29

        lui $a0, 0x1000      # Print number of zeros
        addi $a0, $a0, 28
        addi $v0, $0, 4
        syscall

        add $a0, $s0, $0
        addi $v0, $0, 1
        syscall

        lui $a0, 0x1000      # Print number of ones
        addi $a0, $a0, 66
        addi $v0, $0, 4
        syscall

        add $a0, $s1, $0
        addi $v0, $0, 1
        syscall

        add $ra, $s2, $0     # Restore return address
        jr $ra
        nor $0, $0, $0       # Branch delay slot

count:  # This is where the count code goes
        # The number was passed in a0
        # Before returning, be sure that return values
        # are placed in v0 and v1
   
        jr $ra
        nor $0, $0, $0       # Branch delay slot
                

