#program: hello.asm

	.data
myMsg:	 .asciiz "\nHello, world.\n"

	.text
	.globl main
main:
	li $v0, 4
	la $a0, myMsg
	syscall