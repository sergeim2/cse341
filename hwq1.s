#program: hwq1.asm

.data
xy:     .word 0x0cfe0506
yz:     .word 0x121e00fa
ta:     .word 0x00ae
.text
.globl main
main:   lui $t0, 0x1000
addi $t0, $t0, 1
add $t3,$0,$0
add $v1,$0,$0	#v1 is for counter
lb $t1, ($t0)  #divider
addi $t0,$t0,2 #getting address of next variable
nor $0, $0, $0
lb $t2, ($t0)  #what we are dividing put in t2
nor $0, $0, $0

dloop:	sub $v0, $t2,$t1 #dividing loop
nor $0, $0, $0
bltz $v0, done
addi $v1,$v1,1
sub $t2,$t2,$t1
slt $t3, $t2, $t1 #sets t3 to 1 if big value smaller than divider
bne $t3,$0, done
sub $t2,$t2,$t1
j dloop
done:
nor $0, $0, $0  #t2 = remainder
nor $0, $0, $0	#v1 = ans
nor $0, $0, $0
nor $0, $0, $0
nor $0, $0, $0


