#program: trace1.asm

.data
xy:	.word 0x0cfe0506
yz:	.word 0x121e00fa
ta:	.word 0x00ae
.text
.globl main
main:	lui $t0, 0x1000
	lbu $t1, ($t0)
	addi $t0, $t0, 1 
	lbu $t2, ($t0)
	and $v0, $0, $0
next:	lb $t2, ($t0)
	addi $t0, $t0, 1
	add $v0, $t2, $v0
	slt $t3, $t2, $0
	beq $t3, $0, wrapup
	nor $0, $0, $0
	sub $t2, $0, $t2
	sb $t2, -1($t0)
wrapup:	addi $t1, $t1, -1
	bgtz $t1, next
	nor $0, $0, $0
