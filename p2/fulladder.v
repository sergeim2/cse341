module fulladder(a,b,c,s,cout);
input a,b,c;
output s,cout;
xor #1
g1(w1, a, b),
g2(s, w1, c);
and #1
g3(w2, c, b),
g4(w3, c, a),
g5(w4, a, b);
or #1
g6(cout, w2, w3, w4);
endmodule
