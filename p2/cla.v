//16-bit ripple carry adder
//name:Sergei Mellow
//ID:50030322
//UBNAME:sergeime
`timescale 1ns/1ns
module pfa(
   output  p, g, s,
   input   a, b, cin);
   xor #1 sumer(s, a, b, cin);
   xor #1 proper(p, a, b);
   and #1 gener(g, a, b);
endmodule

module fulladder(a,b,c,s,cout);
input a,b,c;
output s,cout;
xor #1
g1(w1, a, b),
g2(s, w1, c);
and #1
g3(w2, c, b),
g4(w3, c, a),
g5(w4, a, b);
or #1
g6(cout, w2, w3, w4);
endmodule

module CLA_4bit(
    output Cout,
    output [3:0] S,
    output PG,GG,
    input [3:0] A,B,
    input Cin
    );
    wire [3:0] G,P,C;
    wire [19:0]tempwire;
    //assign G = A & B; //Generate
    //assign P = A ^ B; //Propagate
    //C[0]=Cin; //assign C[0] = Cin;
    and #1
    agAS(C[0],Cin), //assign C[0] = Cin;
    agG[3:0](G[3:0],A[3:0],B[3:0]), //generate
    ag1(tempwire[0],P[0],C[0]),
    ag2(tempwire[1],P[0],C[0],P[1]),
    ag3(tempwire[2],P[1],G[0]),
    ag4(tempwire[3],G[1],P[2]),
    ag5(tempwire[4],P[2],P[1],G[0]),
    ag6(tempwire[5],P[2],P[1],P[0],C[0]),
    ag7(tempwire[6],P[3],G[2]),
    ag8(tempwire[7],P[3],P[2],G[1]),
    ag9(tempwire[8],P[3],P[2],P[1],G[0]),
    ag10(tempwire[9],P[3],P[2],P[1],p[0],C[0]),
    ag11(PG,P[3],P[2],P[1],P[0]),
    ag12(tempwire[10],P[3],G[2]),
    ag13(tempwire[11],P[3],P[2],G[1]),
    ag14(tempwire[12],P[3],p[2],p[1],G[0]);
    or #1
    ogP[3:0](P[3:0],A[3:0],B[3:0]), //propagate
    og1(C[1],G[0],tempwire[0]),
    og2(C[2],tempwire[1],tempwire[2],G[1]),
    og3(C[3],tempwire[3],tempwire[4],tempwire[5],G[2]),
    og4(Cout,tempwire[6],tempwire[7],tempwire[8],tempwire[9],G[3]),
    ogs(S[0],P[0],C[0]), //s=p or g
ogs(S[1],P[1],C[1]), //s=p or g
ogs(S[2],P[2],C[2]), //s=p or g
ogs(S[3],P[3],C[3]), //s=p or g

    og5(GG,tempwire[10],tempwire[11],tempwire[12],G[3]);

    //assign C[0] = Cin;
    //assign C[1] = G[0] | (P[0] & C[0]);
    //assign C[2] = G[1] | (P[1] & G[0]) | (P[1] & P[0] & C[0]);
    //assign C[3] = G[2] | (P[2] & G[1]) | (P[2] & P[1] & G[0]) |             (P[2] & P[1] & P[0] & C[0]);
    //assign Cout = G[3] | (P[3] & G[2]) | (P[3] & P[2] & G[1]) | (P[3] & P[2] & P[1] & G[0]) |(P[3] & P[2] & P[1] & P[0] & C[0]);
    //assign S = P ^ C;
    
    //assign PG = P[3] & P[2] & P[1] & P[0];
    //assign GG = G[3] | (P[3] & G[2]) | (P[3] & P[2] & G[1]) | (P[3] & P[2] & P[1] & G[0]);
endmodule
  
module CLA_16bit(
output co,
   output [15:0] s,
   input [15:0] a,b,
   input cin);
//output [15:0] Sum,
//output Cout,
//input [15:0] A,B,
//input Cin
//);
//wire c3,p3,g3,p7,g7;
wire [3:0] carry, p, g;
   wire p_up, g_up;

   wire c1,c2,c3,c4;

   CLA_4bit add1 (c1,s[3:0],p[0],g[0],a[3:0],b[3:0],cin);
   CLA_4bit add2 (c2,s[7:4],p[1],g[1],a[7:4],b[7:4],carry[0]);
   CLA_4bit add3 (c3,s[11:8],p[2],g[2],a[11:8],b[11:8],carry[1]);
   CLA_4bit add4 (c4,s[15:12],p[3],g[3],a[15:12],b[15:12],carry[2]);

    and #1
    agag1(co,carry[3]);
   //assign co = carry[3];
endmodule
//CLA_4bit CLA1(Sum[3:0],c3,p3,g3,A[3:0],B[3:0],Cin);
//CLA_4bit CLA2(Sum[7:4],Cout,p7,g7,A[7:4],B[7:4],c3);
//CLA_4bit CLA3(Sum[11:8],Cout,p7,g7,A[7:4],B[7:4],c3);
///CLA_4bit CLA4(Sum[15:12],Cout,p7,g7,A[7:4],B[7:4],c3);

//endmodule

module Test_CLA_4bit;
    // Inputs
    reg [3:0] A;
    reg [3:0] B;
    reg Cin;

    // Outputs
    wire [3:0] S;
    wire Cout;
    wire PG;
    wire GG;


    initial begin
    // Initialize Inputs
    A = 0; B = 0; Cin = 0;
    // Wait 100 ns for global reset to finish
    #100;
        
    // Add stimulus here
    A=4'b0001;B=4'b0000;Cin=1'b0;
    #10 A=4'b100;B=4'b0011;Cin=1'b0;
    #10 A=4'b1101;B=4'b1010;Cin=1'b1;
    #10 A=4'b1110;B=4'b1001;Cin=1'b0;
    #10 A=4'b1111;B=4'b1010;Cin=1'b0;
    end

    initial begin
$monitor("time=",$time,, "A=%b B=%b Cin=%b : Sum=%b Cout=%b PG=%b GG=%b",A,B,Cin,S,Cout,PG,GG);
    end      
endmodule
/*module four_bit_carry_lookahead (input  [3:0] a, input  [3:0] b,
  input        c,    //Carry in
  output [3:0] s,    //Sum
  output       cout  //Carry
);


  wire [3:1] carry; // 3:1 to align numbers with wikipedia article
  wire [3:0] p;
  wire [3:0] g;

  fulladder add0(a[0], b[0]), c,s[0], cout ,g[0],p[0] );
  fulladder add1(a[1], b[1]), carry[1],s[1], cout ,g[1],p[1]);
  fulladder add2(a[2], b[2]), carry[2],s[2], cout ,g[2],p[2]);
  fulladder add3(a[3], b[3]), carry[3],s[3], cout ,g[3],p[3]);

  carry_lookahead(
   .p    (p    ), //input  [3:0] 
   .g    (g    ), //input  [3:0]
   .c    (carry), //output [3:1]
   .cout (cout )  //output
  );
endmodule

module carry_lookahead (cout, sout, xin, yin, cin);
    input cin;
    input [3:0] xin, yin;
    output [3:0] sout;
    output cout;
endmodule

module sixteen_bit_carry_lookahead()
  wire [3:0] inputa,[3:0] inputb,[3:0] inputc,[3:0] inputd; // 3:1 to align numbers with wikipedia article
  wire [3:0] inputa1,[3:0] inputa2,;[3:0] inputa3,;[3:0] inputa4;
  wire c1,c2,c3,c4;
  wire [3:0] s1,output [3:0] s2,output [3:0] s3,output [3:0] s4;
  wire cout1,cout2,cout3.cout4; 
four_bit_carry_lookahead() fbcl0([3:0] inputa(),[3:0] inputa1,c1,[3:0] s1,cout1);
four_bit_carry_lookahead() fbcl1();
four_bit_carry_lookahead() fbcl2();
four_bit_carry_lookahead() fbcl3();
endmodule
*\
