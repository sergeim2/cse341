module testAdder(a,b,s,cout,cin);
input [15:0] s;
input cout;
output [15:0] a,b;
output cin;
reg [15:0] a,b;
reg cin;
initial
begin
$monitor($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout);
$display($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout);
#20 a=2; b=3; cin=0;
#20 a=1; b=7; cin=0;
#20 // Required for iverilog to show final values
$display($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout);
end
endmodule
