`timescale 1ns/1ns
`include test16adder.v
`include rca.v
module testbench();
wire [15:0] x,y,s;
wire cin,cout;
testAdder test (x,y,s,cout,cin);
sixteenBitAdder adder (x,y,s,cout,cin);
endmodule
