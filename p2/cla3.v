`timescale 1ns/1ns
////////////Test Bench////////////////

module testbench(); 
 wire [15:0] x,y,s; 
 wire cin,cout;     
  testAdder test(x,y,s,cout,cin); 
  add16 adder(cout,s,x,y,cin);
endmodule
module testAdder(a,b,s,cout,cin); 
 input [15:0] s; 
 input cout; 
 output [15:0] a,b; 
 output cin; 
 reg [15:0] a,b; 
 reg cin; 
 initial 
    begin 
  $monitor($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout); 
  $display($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout); 
  #20   a=0; b=0; 
        cin=0; 			
  #20   a=11; b=3; 
        cin=0;	
  #20   a=5; b=14; cin=0;
  #20   a=12; b=15; cin=0;
  #20   a=14; b=12; cin=0;
  #20   a=16; b=11; cin=0;
  #20   a=1; b=14; cin=0;
  #20   a=4; b=251; cin=0;				//Testing limits 
  #20   a=253; b=12; cin=0; 
  #20  // Required for iverilog to show final values        
 $display($time,,"a=%b, b=%b, c=%b, s=%b, cout=%b",a,b,cin,s,cout); 
    end 
endmodule 

///////////// Main Program/////////////////

module pfa(
   output  p, g, s,
   input   a, b, cin);
   xor #1 sum(s, a, b, cin);
   xor #1 prop(p, a, b);
   and #1 gen(g, a, b);
endmodule

//cla module 4 bit
module cla(
   output   p_up, g_up,
   output [3:0]  cout,  
   input  [3:0]  p, g,
   input cin);
   wire p3g2,p3p2g1,p3p2p1g0,p3p2p1p0cin,p2g1,p2p1g0,p2p1p0cin,p1g0,p1p0cin,p0cin;
   
   and #1
   	gate0(p_up, p[0]),

   	gate1(p3g2,p[3],g[2]),
   	gate2(p3p2g1,p[3],p[2],g[1]),
	gate3(p3p2p1g0,p[3],p[2],p[1],g[0]),
   	gate4(p3p2p1p0cin,p[3],p[2],p[1],p[0],cin),
   	gate5(p2g1,p[2],g[1]),
   	gate6(p2p1g0,p[2],p[1],g[0]),
   	gate7(p2p1p0cin,p[2],p[1],p[0],cin),
   	gate8(p1g0,p[1],g[0]),
   	gate9(p1p0cin,p[1],p[0],cin),
   	gate10(p0cin,p[0],cin);
   
   //g_up = g[3] + p[3]&g[2] + p[3]&p[2]&g[1] + p[3]&p[2]&p[1]&g[0];
   or #1 (g_up,g[3],p3g2,p3p2g1,p3p2p1g0);
   
   //cout[3] = g[3] + p[3]&g[2] + p[3]&p[2]&g[1] + p[3]&p[2]&p[1]&g[0] + p[3]&p[2]&p[1]&p[0]&cin;
   or #1(cout[3],g[3],p3g2,p3p2g1,p3p2p1g0,p3p2p1p0cin);
   
   //cout[2] = g[2] + p[2]&g[1] + p[2]&p[1]&g[0] + p[2]&p[1]&p[0]&cin;
   or #1(cout[2], g[2],p2g1,p2p1g0,p2p1p0cin);
   
   //cout[1] = g[1] + p[1]&g[0] + p[1]&p[0]&cin;
   or #1(cout[1],g[1],p1g0,p1p0cin);
   
   //cout[0] = g[0] + p[0]&cin;
   or  #1(cout[0],g[0],p0cin);

endmodule

//put the cla and #1 adders together to make them easier to merge
module adder4_cla(
   output co,
   output [3:0] s,
   output p_up,g_up,
   input [3:0] a,b,
   input cin);

   wire [3:0] p,g;
   wire [3:0] carry;
     
   pfa add0(p[0],g[0],s[0],a[0],b[0],cin);
   pfa add1(p[1],g[1],s[1],a[1],b[1],carry[0]);
   pfa add2(p[2],g[2],s[2],a[2],b[2],carry[1]);
   pfa add3(p[3],g[3],s[3],a[3],b[3],carry[2]);

   cla tempcla(p_up,g_up,carry,p,g,cin);

   and #1(co,carry[3]);
endmodule

module add16(   
   output co,
   output [15:0] s,
   input [15:0] a,b,
   input cin);

   wire [3:0] carry, p, g;
   wire p_up, g_up;

   wire c1,c2,c3,c4;

	adder4_cla add1 (c1,s[3:0],p[0],g[0],a[3:0],b[3:0],cin);
	adder4_cla add2 (c2,s[7:4],p[1],g[1],a[7:4],b[7:4],carry[0]);
	adder4_cla add3 (c3,s[11:8],p[2],g[2],a[11:8],b[11:8],carry[1]);
   	adder4_cla add4 (c4,s[15:12],p[3],g[3],a[15:12],b[15:12],carry[2]);

   	cla fcla(p_up,g_up,carry,p,g,cin);

    and #1(co,carry[3]);
endmodule



