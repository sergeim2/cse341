//4-bit by 4-bit array multiplier (consisting of AND gates and full adders and half adders)  in Verilog HDL.
//name:Sergei Mellow
//ID:50030322
//UBNAME:sergeime
`timescale 1ns/1ns
module testbench();
  wire [7:0]p; 
  wire [3:0] x,y; 
  mult4 mult(p,x,y);     
  testmult test(p,x,y);
 endmodule

module testmult(p,a,b);  
 output [3:0] a,b; 
 input [7:0] p; 
 reg [3:0] a,b; 
 initial 
    begin 
  $monitor($time,,"a=%d, b=%d, p=%d",a,b,p); 
  $display($time,,"a=%d, b=%d, p=%d",a,b,p); 
#80   a=0; b=0;
#80   a=2; b=3; 
#80   a=1; b=7; 
#80   a=8; b=5;
#80   a=6; b=7;
#80   a=11; b=1;
#80   a=0; b=11;
#80   a=5; b=250;
#80   a=33; b=22;
#80   a=133; b=5;
#80   a=27; b=69;
#80   a=53; b=19;
#80   a=43; b=15;
#80   a=24; b=2; 
#80   a=111; b=23;
#80   a=44; b=14; 
#80   a=50; b=90; 
#80   a=100; b=100;
#80   a=15; b=124; 
#80   a=93; b=99; 
#80   a=2; b=42; 
#80   a=11; b=66;
#80   a=17; b=83;
#80   a=10; b=37;
#80   a=200; b=11;
#80   a=2; b=231;
#80   a=244; b=10;
#80   a=240; b=52;
#80   a=11; b=11; 
#80   a=22; b=222;
#80   a=3; b=333; 
#80   a=1; b=1; 
#80   a=99; b=202;
#80   a=2; b=4; 
#80   a=5; b=0; 
#80   a=33; b=9;
#80   a=62; b=1;
#80   a=12; b=1;
#80   a=20; b=91;
#80   a=211; b=5;
#80   a=152; b=4;
#80   a=192; b=3;
#80   a=102; b=2;
#80   a=107; b=1;
#80   a=2; b=172;
#80   a=6; b=1; 
#80   a=41; b=84; 
#80   a=17; b=52; 
#80   a=1; b=162; 
#80   a=253; b=3; 
#80  // Required for iverilog to show final values        
 $display($time,,"a=%b, b=%b, p=%b",a,b,p); 
    end 
endmodule
//special thanks to behavioral code from that i changed to struct code
//http://highered.mcgraw-hill.com/sites/dl/free/0070601755/366087/MultiplierHDL_FPGA.pdf
module HA(sout,cout,a,b);
output sout,cout;
input a,b;
//assign sout=a^b;
//assign cout=(a&b);
xor  xor1(sout,a,b);
and  and1(cout,a,b);
endmodule
module FA(sout,cout,a,b,cin);
wire tempwire[2:0];
output sout,cout;
input a,b,cin;
xor  xor2(sout,a,b,cin);  //struct code
and  and1(tempwire[0],a,b);
and  and2(tempwire[1],a,cin);
and  and3(tempwire[2],cin,b);

or  or1(cout,tempwire[0],tempwire[1],tempwire[2]);

//assign sout=(a^b^cin);
//assign cout=((a&b)|(a&cin)|(b&cin));
endmodule
module mult4(product,inp1,inp2);
output [7:0]product;
input [3:0]inp1;
input [3:0]inp2;
//assign product[0]=(inp1[0]&inp2[0]);
and  and4(product[0],inp1[0],inp2[0]);
wire x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17;
HA HA1(product[1],x1,(inp1[1]&inp2[0]),(inp1[0]&inp2[1]));
FA FA1(x2,x3,inp1[1]&inp2[1],(inp1[0]&inp2[2]),x1);
FA FA2(x4,x5,(inp1[1]&inp2[2]),(inp1[0]&inp2[3]),x3);
HA HA2(x6,x7,(inp1[1]&inp2[3]),x5);
HA HA3(product[2],x15,x2,(inp1[2]&inp2[0]));
FA FA5(x14,x16,x4,(inp1[2]&inp2[1]),x15);
FA FA4(x13,x17,x6,(inp1[2]&inp2[2]),x16);
FA FA3(x9,x8,x7,(inp1[2]&inp2[3]),x17);
HA HA4(product[3],x12,x14,(inp1[3]&inp2[0]));
FA FA8(product[4],x11,x13,(inp1[3]&inp2[1]),x12);
FA FA7(product[5],x10,x9,(inp1[3]&inp2[2]),x11);
FA FA6(product[6],product[7],x8,(inp1[3]&inp2[3]),x10);
endmodule
