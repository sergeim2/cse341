`timescale 1ns/1ns
module fulladder() ;
wire w1, w2, w3, w4, s, cout;
reg a, b, c;
xor 
g1(w1, a, b),
g2(s, w1, c);
and 
g3(w2, c, b),
g4(w3, c, a),
g5(w4, a, b);
or 
g6(cout, w2, w3, w4);
initial
begin
$monitor($time,,,, "a=%b, b=%b, c=%b, s=%b, cout=%b",a,b,c,s,cout);
$display($time,,,, "a=%b, b=%b, c=%b, s=%b, cout=%b",a,b,c,s,cout);
#10 a=0; b=0; c=0;
#10 a=1;
#10 b=1;
#10 c=1; a=0;
#10 a=1;
#10 // Required for iverilog to show final values
$display($time,,,, "a=%b, b=%b, c=%b, s=%b, cout=%b",a,b,c,s,cout);
end
endmodule
