//16-bit RCA
//name:Sergei Mellow
//ID:50030322 
//UBNAME:sergeime
`timescale 1ns/1ns
module fulladder(a,b,c,s,cout);


input a,b,c;
output s,cout;
xor 
g1(w1, a, b),
g2(s, w1, c);
and 
g3(w2, c, b),
g4(w3, c, a),
g5(w4, a, b);
or  
g6(cout, w2, w3, w4);
endmodule

module sixteenBitAdder(x,y,s,cout,cin);  //expanded code from 4bit
input [15:0] x,y;
output [15:0] s;
input cin;
output cout;
wire c[15:0];
fulladder f0 (x[0],y[0],cin,s[0],c[0]);
fulladder f1 (x[1],y[1],c[0],s[1],c[1]);
fulladder f2 (x[2],y[2],c[1],s[2],c[2]);
fulladder f3 (x[3],y[4],c[2],s[3],c[3]);
fulladder f4 (x[4],y[5],c[3],s[4],c[4]);
fulladder f5 (x[5],y[6],c[4],s[5],c[5]);
fulladder f6 (x[6],y[7],c[5],s[6],c[6]);
fulladder f7 (x[7],y[8],c[6],s[7],c[7]);
fulladder f8 (x[8],y[9],c[7],s[8],c[8]);
fulladder f9 (x[9],y[10],c[8],s[9],c[9]);
fulladder f10 (x[10],y[11],c[9],s[10],c[10]);
fulladder f11 (x[11],y[12],c[10],s[11],c[11]);
fulladder f12 (x[12],y[13],c[11],s[12],c[12]);
fulladder f13 (x[13],y[14],c[12],s[13],c[13]);
fulladder f14 (x[14],y[15],c[13],s[14],c[14]);
fulladder f15 (x[15],y[15],c[14],s[15],cout);
endmodule

module testAdder(a,b,s,cout,cin);  //expanded code from 4bit
input [15:0] s;
input cout;
output [15:0] a,b;
output cin;
reg [15:0] a,b;
reg cin;
initial
begin
$monitor($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout);
$display($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout);
#80   a=0; b=0; cin=0;
#80   a=2; b=3; cin=0;
#80   a=1; b=7; cin=0;
#80   a=8; b=5; cin=0;  
#80   a=6; b=7; cin=0;
#80   a=11; b=1; cin=0;
#80   a=0; b=11; cin=0;
#80   a=1; b=250; cin=0;
#80   a=33; b=22; cin=0;
#80   a=133; b=5; cin=0;
#80   a=27; b=69; cin=0;
#80   a=53; b=19; cin=0;
#80   a=43; b=15; cin=0;
#80   a=24; b=2; cin=0;
#80   a=111; b=23; cin=0;
#80   a=44; b=14; cin=0;
#80   a=50; b=90; cin=0;
#80   a=100; b=100; cin=0;
#80   a=15; b=124; cin=0;
#80   a=93; b=99; cin=0;
#80   a=2; b=42; cin=0;
#80   a=11; b=66; cin=0;
#80   a=17; b=83; cin=0;
#80   a=10; b=37; cin=0;
#80   a=200; b=11; cin=0;
#80   a=2; b=231; cin=0;
#80   a=244; b=10; cin=0;
#80   a=240; b=52; cin=0;
#80   a=11; b=11; cin=0;
#80   a=22; b=222; cin=0;
#80   a=3; b=333; cin=0;
#80   a=1; b=1; cin=0;
#80   a=99; b=202; cin=0;
#80   a=2; b=4; cin=0;
#80   a=5; b=0; cin=0;
#80   a=33; b=9; cin=0;
#80   a=62; b=1; cin=0;
#80   a=12; b=1; cin=0;
#80   a=20; b=91; cin=0;
#80   a=211; b=5; cin=0;
#80   a=152; b=4; cin=0;
#80   a=192; b=3; cin=0;
#80   a=102; b=2; cin=0;
#80   a=107; b=1; cin=0;
#80   a=2; b=172; cin=0;
#80   a=6; b=1; cin=0;
#80   a=41; b=84; cin=0;
#80   a=17; b=52; cin=0;
#80   a=1; b=162; cin=0;
#80   a=253; b=3; cin=0;
#80  // Required for iverilog to show final values
$display($time,,"a=%b, b=%b, c=%b, s=%b, cout=%b",a,b,cin,s,cout);
end
endmodule

module testbench(); //expanded code from 4bit
 wire [15:0] x,y,s; 
 wire cin,cout;     
  testAdder test(x,y,s,cout,cin); 
  sixteenBitAdder adder(x,y,s,cout,cin); 
endmodule
