//sergei mellow
// project 2
// 50030322
	//due nov 30
`timescale 1ns/1ns
module partialFullAdder(output p, g, s, input a, b, cin);
    and  gen(g, a, b);
    xor   pro(p, a, b);
    xor   sum(s, a, b, cin);
endmodule

module testbench(); 
 wire [15:0] x,y,z; 
 wire cin,cout;     
  testAdder t(x,y,z,cout,cin); 
  add16 ad(cout,z,x,y,cin);
endmodule
module testAdder(a,b,s,cout,cin); 
 input [15:0] s; 
 input cout; 
 output [15:0] a,b; 
 output cin; 
 reg [15:0] a,b; 
 reg cin; 
 initial 
  begin 
  $monitor($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout); 
  $display($time,,"a=%d, b=%d, c=%b, s=%d, cout=%b",a,b,cin,s,cout); 
  #80   a=0; b=0; cin=0; // 50 inputs as requested			
  #80   a=4; b=1; cin=0;	
  #80   a=7; b=2; cin=0;
 #80   a=1; b=7; cin=0;
 #80   a=8; b=5; cin=0;  
 #80   a=6; b=7; cin=0;
 #80   a=11; b=1; cin=0;
 #80   a=0; b=11; cin=0;
 #80   a=5; b=250; cin=0;
 #80   a=33; b=22; cin=0;
 #80   a=133; b=5; cin=0;
 #80   a=27; b=69; cin=0;
 #80   a=53; b=19; cin=0;
 #80   a=43; b=15; cin=0;
 #80   a=24; b=2; cin=0;
 #80   a=111; b=23; cin=0;
 #80   a=44; b=14; cin=0;
 #80   a=50; b=90; cin=0;
 #80   a=100; b=100; cin=0;
 #80   a=15; b=124; cin=0;
 #80   a=93; b=99; cin=0;
 #80   a=2; b=42; cin=0;
 #80   a=11; b=66; cin=0;
 #80   a=17; b=83; cin=0;
 #80   a=10; b=37; cin=0;
 #80   a=200; b=11; cin=0;
 #80   a=2; b=231; cin=0;
 #80   a=244; b=10; cin=0;
 #80   a=240; b=52; cin=0;
 #80   a=11; b=11; cin=0;
 #80   a=22; b=222; cin=0;
 #80   a=3; b=333; cin=0;
#80   a=1; b=1; cin=0;
 #80   a=99; b=202; cin=0;
 #80   a=2; b=4; cin=0;
 #80   a=5; b=0; cin=0;
 #80   a=33; b=9; cin=0;
 #80   a=62; b=1; cin=0;
#80   a=12; b=1; cin=0;
 #80   a=20; b=91; cin=0;
 #80   a=211; b=5; cin=0;
 #80   a=152; b=4; cin=0;
#80   a=192; b=3; cin=0;
 #80   a=102; b=2; cin=0;
 #80   a=107; b=1; cin=0;
 #80   a=2; b=172; cin=0;
 #80   a=6; b=1; cin=0;
 #80   a=41; b=84; cin=0;
 #80   a=66; b=2; cin=0;
#80   a=252; b=6; cin=0;				
#80   a=300; b=80; cin=0; 
  #80  // Required for iverilog to show final values        
 $display($time,,"a=%b, b=%b, c=%b, s=%b, cout=%b",a,b,cin,s,cout); 
    end 
endmodule 

//CLA 4 BIT
module cla(output pUP, gUP, output [3:0]  cout, input  [3:0]  p, g, input cin);
   //found lots of code online... did a little cleaning.
// credits to
//http://trixy.justinkbeck.com/2007/09/structural-16bit-cla-adder-in-verilog.html 

	wire p3g2, p3p2g1, p3p2p1g0, p3p2p1p0cin;
	and (pUP, p[0]);
	and (p3g2,p[3],g[2]);
	and (p3p2g1,p[3],p[2],g[1]);
	and (p3p2p1g0,p[3],p[2],p[1],g[0]);
	and (p3p2p1p0cin,p[3],p[2],p[1],p[0],cin);
	or (gUP,g[3],p3g2,p3p2g1,p3p2p1g0);
	or (cout[3],g[3],p3g2,p3p2g1,p3p2p1g0,p3p2p1p0cin);
	wire p2g1,p2p1g0,p2p1p0cin,p1g0,p1p0cin,p0cin;
	and (p2g1,p[2],g[1]);
	and (p2p1g0,p[2],p[1],g[0]);
	and (p2p1p0cin,p[2],p[1],p[0],cin);
	and (p1g0,p[1],g[0]);
 	and (p1p0cin,p[1],p[0],cin);
	and (p0cin,p[0],cin);
	or (cout[2], g[2],p2g1,p2p1g0,p2p1p0cin);
	or (cout[1],g[1],p1g0,p1p0cin);
   	or (cout[0],g[0],p0cin);
endmodule

//4 4bit cla's webbed together with CLA logic 2 level
module add16(output cout,output [15:0] s,input [15:0] a,b,input cin);
        wire [3:0] carry, p, g;
        wire pUP, gUP;
        wire c1,c2,c3,c4;
	adder4_cla a1 (c1,s[3:0],p[0],g[0],a[3:0],b[3:0],cin);
	adder4_cla a2 (c2,s[7:4],p[1],g[1],a[7:4],b[7:4],carry[0]);
	adder4_cla a3 (c3,s[11:8],p[2],g[2],a[11:8],b[11:8],carry[1]);
	adder4_cla a4 (c4,s[15:12],p[3],g[3],a[15:12],b[15:12],carry[2]);
	cla finalcla(pUP,gUP,carry,p,g,cin);
	and (cout,carry[3]); 
	endmodule
//combines cla and adders together so they can merge
 module adder4_cla(
         output co,
         output [3:0] s,
         output pUP,gUP,
         input [3:0] a,b,
         input cin);
         wire [3:0] p,g;
         wire [3:0] carry;
         partialFullAdder a1(p[0],g[0],s[0],a[0],b[0], cin);
         partialFullAdder a2(p[1],g[1],s[1],a[1],b[1], carry[0]);
         partialFullAdder a3(p[2],g[2],s[2],a[2],b[2], carry[1]);
         partialFullAdder a4(p[3],g[3],s[3],a[3],b[3], carry[2]);
         cla localcla(pUP,gUP,carry,p,g,cin);
         and
         ag4b(c0,carry[3]);
         endmodule

