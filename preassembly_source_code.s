.globl main		# Run spim or xspim in bare mode (spim -bare or xspim -bare)
.globl rtn		# Set a breakpoint at rtn (return) to view results in memory and registers
.data			# When initializing test data be sure the result does not overflow a byte
x:	.byte	3	
y:	.byte	7
.text
main:
	lui	$t0, 0x1000
	lb	$s0, ($t0)	# Load x
	lb	$s1, 1($t0)	# Load y
	sll	$s2, $s0, 6	# x*64
	add	$s2, $s2, $s0	# x * 65
	sub	$s3, $s2, $s1	# x*65-y
	addi	$s4, $s3, 39	# x*65-y+39
	sb	$s4, 1($t0)	# Store y
rtn:  	jr	$ra		# Return
	